#!/usr/bin/env python
import logging
import sys
from asyncio import BaseEventLoop
from os.path import join, realpath
from typing import Optional

from komoutils.logger.struct_logger import METRICS_LOG_LEVEL

from ftso_setup_and_configuration.data_sources.crypto import Crypto
from ftso_setup_and_configuration.data_sources.trading_metadata import TradingMetadata
from ftso_setup_and_configuration.oracle_setup import OracleSetup

sys.path.insert(0, realpath(join(__file__, "../../../../")))

import asyncio
import unittest

logging.basicConfig(level=METRICS_LOG_LEVEL)


class TrainingMetadataUnitTest(unittest.TestCase):
    symbols: list = []

    @classmethod
    def setUpClass(cls):
        cls.ev_loop: BaseEventLoop = asyncio.get_event_loop()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    # def test_main(self):
    #     setup: OracleSetup = OracleSetup()
    #     metadata = TradingMetadata(setup=setup)
    #     metadata.derive_all()
    #
    #     for name, category in metadata.categories.items():
    #         for bucket, symbols in category.buckets.items():
    #             print(f"{name} category bucket {bucket} has {len(symbols)} symbols.")
    #
    #         print(f"Found {len(category.buckets)} symbols. ")

    def test_get_symbols(self):
        setup: OracleSetup = OracleSetup()
        setup_pack = {
            "url": setup.third_party_services["coinapi"]["url"],
            "key": setup.third_party_services["coinapi"]["key"],
            "exchanges": setup.exchanges,
            "assets": setup.assets,
            "quotes": setup.quotes,
            "environments": setup.environments,
        }
        metadata = TradingMetadata(setup=setup)
        crypto: Crypto = metadata.categories['crypto']
        crypto.derive()

        # print(f"Found {len(metadata.symbols)} symbols. ")
        # print(metadata.symbols)
