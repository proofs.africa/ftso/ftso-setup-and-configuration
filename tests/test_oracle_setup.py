#!/usr/bin/env python
import logging
import sys
from asyncio import BaseEventLoop
from os.path import join, realpath
from typing import Optional

from komoutils.logger.struct_logger import METRICS_LOG_LEVEL

from ftso_setup_and_configuration.data_sources.trading_metadata import TradingMetadata
from ftso_setup_and_configuration.oracle_setup import OracleSetup

sys.path.insert(0, realpath(join(__file__, "../../../../")))

import asyncio
import unittest

logging.basicConfig(level=METRICS_LOG_LEVEL)


class TrainingMetadataUnitTest(unittest.TestCase):
    symbols: list = []

    @classmethod
    def setUpClass(cls):
        cls.ev_loop: BaseEventLoop = asyncio.get_event_loop()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_main(self):
        setup: OracleSetup = OracleSetup()
        setup.start()

        self.ev_loop.run_until_complete(asyncio.sleep(5))
