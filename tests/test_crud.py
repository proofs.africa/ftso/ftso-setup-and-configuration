#!/usr/bin/env python
import logging
import sys
from asyncio import BaseEventLoop
from os.path import join, realpath
from typing import Optional

from komoutils.logger.struct_logger import METRICS_LOG_LEVEL

from ftso_setup_and_configuration.data_sources.trading_metadata import TradingMetadata
from ftso_setup_and_configuration.db.crud import Crud
from ftso_setup_and_configuration.oracle_setup import OracleSetup

sys.path.insert(0, realpath(join(__file__, "../../../../")))

import asyncio
import unittest

logging.basicConfig(level=METRICS_LOG_LEVEL)


class CrudUnitTest(unittest.TestCase):
    symbols: list = []

    @classmethod
    def setUpClass(cls):
        cls.ev_loop: BaseEventLoop = asyncio.get_event_loop()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_main(self):
        environment = "development"
        setup: OracleSetup = OracleSetup()
        crud: Crud = Crud(uri=setup.environments[environment]["databases"]["mongo"]["uri"],
                          db_name=setup.environments[environment]["databases"]["mongo"]["name"])

        crud.write_symbol_data(symbols=[{"name": "hello"}])
