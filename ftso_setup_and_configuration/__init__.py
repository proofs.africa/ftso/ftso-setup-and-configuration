# Read setup file from DO
import asyncio
import logging
import os
from typing import Optional

import apconfig
import requests
from aporacle.data.db.crud import Crud
from komoutils.logger import KomoLogger

logging.basicConfig(level=logging.INFO)

app_logger: Optional[KomoLogger] = KomoLogger(name="main")

if "ORACLE_SETUP_FILE" not in os.environ:
    raise Exception("ORACLE_SETUP_FILE access key MUST be set. ")

if "ENVIRONMENT" not in os.environ:
    raise Exception("ENVIRONMENT access key MUST be set. ")

import os
from typing import List


def validate_environment_variable(variable_name: str, allowed_values: List[str]) -> None:
    """
    This function checks if an environment variable exists and its value is one of the allowed values.

    Args:
        variable_name: The name of the environment variable to check.
        allowed_values: A list of allowed values for the environment variable.

    Raises:
        AssertionError: If the environment variable is not set or its value is not in the allowed list.
    """

    value = os.getenv(variable_name)
    assert value is not None, f"Environment variable '{variable_name}' is not set."
    assert value in allowed_values, f"Environment variable '{variable_name}' value '{value}' is not allowed. Valid options are: {', '.join(allowed_values)}"


# Example usage
allowed_values = ["production", "staging", "development"]
validate_environment_variable("ENVIRONMENT", allowed_values)


#
# if "ENVIRONMENT" not in os.environ:
#     raise Exception("ENVIRONMENT tag MUST be set. ")


def get_setup_detail():
    try:
        with requests.Session() as session:
            response = session.get(url=f"{os.getenv('ORACLE_SETUP_FILE')}")
            response.raise_for_status()
            # print(response.json())
            # print(os.getenv('ORACLE_SETUP_FILE'))
            return response.json()

    except Exception as e:
        raise Exception(f"Error while accessing setup file at {os.getenv('ORACLE_SETUP_FILE')}. "
                        f"Ensure network connection exists and that the file exists with correct authentication. "
                        f"More detail -- {e}. ")


def get_symbols():
    crud: Crud = Crud(uri=apconfig.mongo_database_uri, db_name=apconfig.mongo_database_name)
    records = crud.read_from_data(collection='tso_symbols', filters={}, omit={})

    symbols: list = []
    for record in records:
        symbols.extend(record["symbols"])

    print(symbols)


def asset_quote_exchange_mappings(url: str):
    try:
        with requests.Session() as session:
            print(f"A - {url}")
            response = session.get(url=f"{url}")
            response.raise_for_status()
            return response.json()

    except Exception as e:
        raise Exception(f"Error while accessing exchange mapping at {url}. "
                        f"Ensure network connection exists and that the file exists with correct authentication. "
                        f"More detail -- {e}. ")
