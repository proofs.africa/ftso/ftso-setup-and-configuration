import asyncio
import logging
import os
from typing import Optional

from komoutils.core import safe_ensure_future, KomoBase
from komoutils.core.time import the_time_in_iso_now_is
from komoutils.logger import KomoLogger

from ftso_setup_and_configuration import get_setup_detail, get_symbols
from ftso_setup_and_configuration.data_sources.trading_metadata import TradingMetadata


class OracleSetup(KomoBase):
    _logger: Optional[KomoLogger] = None

    @classmethod
    def logger(cls) -> KomoLogger:
        if cls._logger is None:
            cls._logger = logging.getLogger(__name__)
        return cls._logger

    def log_with_clock(self, log_level: int, msg: str, **kwargs):
        self.logger().log(log_level, f"{self.__class__.__name__} {msg} [clock={str(the_time_in_iso_now_is())}]",
                          **kwargs)

    def __init__(self, interval: int = 60):
        self.interval = interval
        self.setup = get_setup_detail()
        self.oracle_setup_task: Optional[asyncio.Task] = None
        self.setup = get_setup_detail()
        self.metadata: Optional[TradingMetadata] = None
        self._symbols: list = get_symbols()

    def start(self):
        self.log_with_clock(log_level=logging.INFO, msg=f"Oracle setup starting. ")
        # setup_pack = {
        #     "url": self.third_party_services["coinapi"]["url"],
        #     "key": self.third_party_services["coinapi"]["key"],
        #     "exchanges": self.exchanges,
        #     "assets": self.assets,
        #     "quotes": self.quotes,
        #     "environments": self.environments,
        # }
        # self.metadata = TradingMetadata(setup=self)
        # self.metadata.derive_all()
        self.oracle_setup_task = safe_ensure_future(self.oracle_setup_loop())
        self.log_with_clock(log_level=logging.INFO, msg=f"Oracle setup successfully started. ")

    def stop(self):
        if self.oracle_setup_task is not None:
            self.oracle_setup_task.cancel()
            self.oracle_setup_task = None

    async def oracle_setup_loop(self):
        while True:
            try:
                await asyncio.sleep(self.interval)
                self.setup = get_setup_detail()
                self._symbols = get_symbols()
                self.log_with_clock(log_level=logging.INFO,
                                    msg=f"Oracle setup successfully updated {os.getenv('ORACLE_SETUP_FILE')}. ")
                continue
            except Exception as e:
                self.log_with_clock(log_level=logging.ERROR,
                                    msg=f"An error occurred while trying to update oracle setup. {e}")

            await asyncio.sleep(self.interval)
            continue

    @property
    def algorithms(self):
        return self.setup["applications"]

    @property
    def applications(self):
        return self.setup["applications"]["algorithms"]

    @property
    def ml(self):
        return self.setup["applications"]["ml"]

    @property
    def tso(self):
        return self.setup["applications"]["tso"]

    @property
    def exchanges(self):
        return self.setup["exchanges"]["venues"]

    @property
    def symbols(self):
        return self._symbols

    @property
    def assets(self):
        return self.setup["exchanges"]["assets"]

    @property
    def quotes(self):
        return self.setup["exchanges"]["quotes"]

    @property
    def chains(self):
        return self.setup["chains"]

    @property
    def notifiers(self):
        notifiers = self.setup["notifiers"]
        return notifiers

    @property
    def third_party_services(self):
        return self.setup["third_party_services"]

    @property
    def environments(self):
        environments = self.setup["environments"]
        return environments
