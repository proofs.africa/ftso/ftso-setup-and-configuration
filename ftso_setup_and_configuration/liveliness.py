from ftso_setup_and_configuration import app_logger


class Liveliness():
    _lle_shared_instance: "Liveliness" = None

    @classmethod
    def get_instance(cls) -> "Liveliness":
        if cls._lle_shared_instance is None:
            cls._lle_shared_instance = Liveliness()
        return cls._lle_shared_instance

    def __init__(self):
        super().__init__()
        self._critical_errors: list = []
        self._service_signaler_interval: int = 15

    @property
    def critical_error_count(self):
        return len(self._critical_errors)

    @property
    def critical_error_do_not_exists(self):
        return False if len(self._critical_errors) > 0 else True

    def set_critical_error(self, ex: BaseException):
        app_logger.error(f"Error has been added to the liveliness probe. {ex}. ")
        self._critical_errors.append(ex)
