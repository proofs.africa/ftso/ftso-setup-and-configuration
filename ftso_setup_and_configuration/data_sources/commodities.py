from komoutils.core import KomoBase


class Commodities(KomoBase):
    def __init__(self, setup: KomoBase) -> None:
        self.category_key = 3
        self.exchange_holder = "COMMODITIES"
        self.quote_holder = "USD"
        self.data_api = "CURRENCYLAYER"
        self.assets = [str(item["name"]).split('/')[0] for item in setup.chains["initial_feeds"] if
                       item["category"] in [self.category_key]]
        self.buckets = {}
        print(self.assets)


    @property
    def name(self):
        return "commodities_category"

    def derive(self):
        symbols = []
        [symbols.append(f'{self.exchange_holder}_SPOT_{asset}_{self.quote_holder}_{self.category_key}_{self.data_api}')
         for asset in self.assets]
        self.buckets = {"bucket_0": symbols}
        # print(symbols)
