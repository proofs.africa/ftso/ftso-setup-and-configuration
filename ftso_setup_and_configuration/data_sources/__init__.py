from pydantic import BaseModel


class Symbol(BaseModel):
    name: str
    volume: float
    weight: float = 0


def sort_by_weight(symbols):
    """Sorts a list of symbols by weight in descending order."""
    return sorted(symbols, key=lambda s: s.weight, reverse=True)


def get_bucket_weights(assigned_symbols):
    """Calculates the combined weight in each bucket."""
    bucket_weights = [sum(s.weight for s in bucket) for bucket in assigned_symbols]
    return bucket_weights


def assign_to_bucket(symbols, buckets):
    """
  Assigns symbols from the list to buckets, prioritizing least combined weight.

  Args:
      symbols: List of Symbol objects representing symbols waiting.
      buckets: List representing buckets.

  Returns:
      A list where each element represents symbols assigned to a specific bucket.
  """
    assigned_symbols = [[] for _ in buckets]  # List to store assigned symbols per bucket
    while symbols:
        symbol = symbols.pop(0)  # Get the next symbol in line
        # Find bucket with the least combined weight
        bucket_weights = get_bucket_weights(assigned_symbols)
        min_weight_index = bucket_weights.index(min(bucket_weights))

        assigned_symbols[min_weight_index].append(symbol)

    return assigned_symbols
