import logging
import os
from typing import List

import pandas as pd
import requests
from komoutils.core import KomoBase

from ftso_setup_and_configuration.data_sources import Symbol, assign_to_bucket


class Crypto(KomoBase):
    def __init__(self, setup: KomoBase) -> None:
        self.setup: KomoBase = setup
        self.url = setup.third_party_services["coinapi"]["url"]
        self.key = setup.third_party_services["coinapi"]["key"]
        self.exchanges = setup.exchanges

        self.quotes = setup.quotes

        self.category_key = 1
        self.data_api = "COINAPI"
        self.assets = []
        self.buckets = {}

        self.initialize()

    @property
    def name(self):
        return "crypto_category"

    def initialize(self):
        if os.getenv("ENVIRONMENT") in ['production']:
            chains = ['flare', 'songbird']
        else:
            chains = ['coston']

        # self.assets = [str(item["name"]).split('/')[0] for item in self.setup.chains["initial_feeds"] if
        #                item["category"] in [1] and len(set(chains) & set(item["chains"])) > 0]

        self.assets = self.setup.assets

        self.log_with_clock(log_level=logging.INFO, msg=f"The following assets will be added to buckets: {self.assets}")

    def get_crypto_symbols_data_by_exchange(self, exchange_id: str):
        try:
            self.log_with_clock(log_level=logging.INFO, msg=f"Sourcing records for {exchange_id}. ")
            payload = {}
            headers = {
                'Accept': 'application/json',
                'X-CoinAPI-Key': self.key
            }

            response = requests.request("GET", f'{self.url}/symbols/{exchange_id}', headers=headers, data=payload)
            print(len(response.json()))
            data: list = []
            for result in response.json():
                print(result)

                if 'asset_id_base' not in result:
                    continue
                if 'asset_id_quote' not in result:
                    continue
                if 'volume_1mth_usd' not in result:
                    continue


                # self.assets = ['BTC']

                if result['asset_id_base'] in self.assets and result['asset_id_quote'] in self.quotes:

                    if str(result['symbol_type']).upper() not in ['SPOT']:
                        continue
                    if len(str(result['symbol_id']).split("_")) > 4:
                        continue

                    data.append({
                        'symbol': f"{result['symbol_id']}_{self.category_key}_{self.data_api}",
                        'exchange': result['exchange_id'],
                        'base': result['asset_id_base'],
                        'quote': result['asset_id_quote'],
                        'volume': result['volume_1mth_usd']
                    })

            self.log_with_clock(log_level=logging.INFO, msg=f"Found {len(data)} symbol records for {exchange_id}. ")
            return data
        except Exception as e:
            self.log_with_clock(log_level=logging.ERROR, msg=f"{exchange_id} {e}. {result}")
            self.errors.append(exchange_id)
            # raise

    def derive(self):
        symbol_records: list = []
        for exchange in sorted(list(self.exchanges.keys())):
            # if exchange not in ['BITRUE', 'HUOBI', 'BYBIT'][0:1]:
            #     continue

            r = self.get_crypto_symbols_data_by_exchange(exchange_id=exchange)
            symbol_records.extend(r)

        # df = pd.DataFrame(data=symbol_records)
        # df.sort_values('volume', ascending=False, inplace=True)
        # print(df)
        #
        # return

        bases = set([symbol['base'] for symbol in symbol_records])
        self.log_with_clock(log_level=logging.INFO, msg=f"Found {len(symbol_records)} across {len(bases)} bases.")

        total_volume = sum([symbol['volume'] for symbol in symbol_records])
        symbols: List[Symbol] = \
            [Symbol(name=symbol['symbol'], volume=symbol['volume'], weight=(symbol['volume'] / total_volume) * 100)
             for symbol in symbol_records]

        buckets = [f"Bucket {i}" for i in range(10)]  # Example: list of bucket names

        assigned_symbols = assign_to_bucket(symbols.copy(), buckets.copy())

        self.buckets = {f"bucket_{i}": [symbol.name for symbol in symbols] for i, symbols in
                        enumerate(assigned_symbols)}

        self.log_with_clock(log_level=logging.INFO, msg=f"{len(assigned_symbols)} records have been derived. ")
