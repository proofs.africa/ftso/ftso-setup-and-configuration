import logging
import random
from typing import Optional, List

import requests
from komoutils.core import KomoBase
from komoutils.core.time import the_time_in_iso_now_is
from komoutils.logger import KomoLogger

from ftso_setup_and_configuration.data_sources import Symbol, assign_to_bucket
from ftso_setup_and_configuration.data_sources.commodities import Commodities
from ftso_setup_and_configuration.data_sources.crypto import Crypto
from ftso_setup_and_configuration.data_sources.currencies import Forex
from ftso_setup_and_configuration.db.crud import Crud


class TradingMetadata:
    _logger: Optional[KomoLogger] = None

    @classmethod
    def logger(cls) -> KomoLogger:
        if cls._logger is None:
            cls._logger = logging.getLogger(__name__)
        return cls._logger

    def log_with_clock(self, log_level: int, msg: str, **kwargs):
        self.logger().log(log_level, f"{self.name} {msg} [clock={str(the_time_in_iso_now_is())}]", **kwargs)

    def __init__(self, setup: KomoBase, environment: str = "development"):

        self.categories = {
            'crypto': Crypto(setup=setup),
            # 'forex': Forex(setup=setup),
            # 'commodities': Commodities(setup=setup)
        }
        # self.assets = {}
        # self.assets['currencies'] = [str(item["name"]).split('/')[0] for item in setup.chains["initial_feeds"] if item["category"] in [2]]
        # self.assets['commodities'] = [str(item["name"]).split('/')[0] for item in setup.chains["initial_feeds"] if item["category"] in [3]]

        self.symbols: dict = {}

        self.errors: list = []
        self.existing: list = []

        # self.crud: Crud = Crud(uri=setup.environments[environment]["databases"]["mongo"]["uri"],
        #                        db_name=setup.environments[environment]["databases"]["mongo"]["name"])

    @property
    def name(self):
        return "metadata"

    def derive_all(self):
        [category.derive() for category in self.categories.values()]

        for name, category in self.categories.items():
            self.symbols[name] = category.buckets
