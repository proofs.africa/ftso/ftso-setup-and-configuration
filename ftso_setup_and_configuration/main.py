import logging

from fastapi import FastAPI, HTTPException

from ftso_setup_and_configuration.liveliness import Liveliness
from ftso_setup_and_configuration.oracle_setup import OracleSetup

logging.basicConfig(level=logging.INFO)

app = FastAPI()

oracle_setup: OracleSetup = OracleSetup()
oracle_setup.start()


@app.get("/")
async def root():
    return {"message": "This is the oracle setup api. "}


@app.get("/fake_an_error", status_code=200)
def fake_an_error():
    Liveliness.get_instance().set_critical_error(Exception("Fake Error. "))
    return {}


@app.get("/health", status_code=200)
def get_service_health():
    if Liveliness.get_instance().critical_error_do_not_exists:
        return {}

    raise HTTPException(status_code=400, detail="Critical error has occurred. ")


@app.get("/setup")
async def entire_setup():
    return {
        "applications": oracle_setup.applications,
        "algorithms": oracle_setup.algorithms,
        "ml": oracle_setup.ml,
        "exchanges": oracle_setup.exchanges,
        "symbols": [],
        "chains": oracle_setup.chains,
        "assets": oracle_setup.assets,
        "tso": oracle_setup.tso,
        "third_party_services": oracle_setup.third_party_services,
        "environments": oracle_setup.environments,
    }


@app.get("/applications")
async def applications():
    return oracle_setup.applications


@app.get("/algorithms")
async def algorithms():
    return oracle_setup.algorithms


@app.get("/ml")
async def ml():
    return oracle_setup.ml


@app.get("/exchanges")
async def exchanges():
    return oracle_setup.exchanges


@app.get("/assets")
async def assets():
    return oracle_setup.assets


@app.get("/quotes")
async def quotes():
    return oracle_setup.quotes


@app.get("/symbols")
async def symbols():
    return oracle_setup.metadata.symbols


@app.get("/chains")
async def chains():
    return oracle_setup.chains


@app.get("/tso")
async def tso():
    return oracle_setup.tso


@app.get("/third_party_services")
async def third_party_services():
    return oracle_setup.third_party_services


@app.get("/environments")
async def environments():
    return oracle_setup.environments
