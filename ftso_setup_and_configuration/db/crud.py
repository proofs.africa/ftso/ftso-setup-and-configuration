from komoutils.db.mongodb_reader_writer import MongoDBReaderWriter


class Crud(MongoDBReaderWriter):
    def __init__(self, uri: str, db_name: str):
        super().__init__(uri, db_name)

    def write_symbol_data(self, symbols):
        self.write(collection=f"symbol_data", data=symbols)

    def read_symbol_data(self):
        return self.read(collection=f"symbol_data", filters={}, omit={"_id": 0})
